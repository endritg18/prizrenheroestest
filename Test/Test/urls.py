"""Test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home'
    ]]=]\)
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf.urls import url, include

from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from products.views import (ProductListView, product_list_view, ProductDetailView,product_detail_view,)
from .view import home_page,about,contact,login_page,register,donate,donatemoney,donateclothes,take
from products.views import (ProductListView, product_list_view, ProductDetailView,product_detail_view,PostListView)
from .view import home_page,about,contact,login_page,register,donate,donatemoney,take


urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', home_page, name='home'),
    path('take/', take, name='take'),
    path('about/', about, name='about'),
    path('donate/', donate, name='donate'),
    path('donateclothes/', donateclothes, name='donateclothes'),
    path('donatemoney/', donatemoney, name='donatemoney'),
    path('contact/', contact, name='contact'),
    path('login/', login_page, name='login'),
    path('register/', register, name='register'),
    path('products/', ProductListView.as_view(),name='products'),
    path('postlistview/', PostListView,name='Post'),
    url(r'^products-fbv/$', product_list_view),
    url(r'^products/(?P<pk>\d+)/$', ProductDetailView.as_view(), name='detail'),
    url(r'^products-fbv/(?P<pk>\d+)/$', product_detail_view),
    #url(r'^', PostListView.as_view (queryset=Post.objects.all().order_by("-created")[:2],),
    path('', include('products.urls')),
]


if settings.DEBUG:

    urlpatterns = urlpatterns + static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)
    urlpatterns = urlpatterns + static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)