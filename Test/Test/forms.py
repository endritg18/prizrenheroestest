from django import forms
from django.contrib.auth import get_user_model

User = get_user_model() #krijimi i objektit



class ContactForm (forms.Form):
    name = forms.CharField()
    email = forms.EmailField()


    def clean_email(self):
        email=self.cleaned_data.get('email')
        if not 'gmail.com' in email:
            raise forms.ValidationError('Perdor vetem Gmail')
        return  email

class Login(forms.Form):
    username= forms.CharField()
    password= forms.CharField(widget=forms.PasswordInput)

class Register(forms.Form):
    username = forms.CharField()
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password is confirmation ',widget=forms.PasswordInput)

    def clean_username(self):
        username = self.cleaned_data.get('username')
        qs = User.objects.filter(username=username)
        if qs.exists():
            raise forms.ValidationError('Name is Taken')
        return username

    def clean(self):
        data = self.cleaned_data
        password = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('password2')
        if password2 != password:
             raise forms.ValidationError ('Match Password')

        return data