from django.shortcuts import render,redirect
from.forms import ContactForm,Login,Register
from django.contrib.auth import authenticate,login,get_user_model

def home_page(request):
    contaxt = {

        'title': 'this is home'
        }
    if request.user.is_authenticated:
        contaxt['in'] = 'Successfully log in'

    return render(request,'home_page.html',contaxt)

def about(request):
    contaxt = {

    'title':'this is about'

        }
    return render(request,'home_page.html',contaxt)


def contact(request):

    contact_form= ContactForm(request.POST or None)
    contaxt = {

        'title': 'this is contact Page',
        'form': contact_form
    }
    if contact_form.is_valid():
        print(contact_form.cleaned_data)

    return render(request,'contact/contact.html', contaxt)



def login_page(request):
    form = Login(request.POST or None)
    contaxt = {

      'form': form
    }
    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form .cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        print(request.user.is_authenticated)
        if username is not None:
           login(request,user)
           contaxt['form'] = Login()
           return redirect('/home')
        else:
         print('Error')
    return render(request,'auth/login.html',contaxt)



User= get_user_model()
def register(request):
    form = Register(request.POST or None)
    contaxt = {

        'form': form
    }
    if form.is_valid():
        username = form.cleaned_data.get('username')
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')
        new_user = User.objects.create_user (username,email,password)
        print(new_user)

    return render(request,'auth/register.html',contaxt)

def donate(request):
    contaxt = {

    'title':'this is Donate'

        }
    return render(request,'donate.html',contaxt)


def donatemoney(request):
    contaxt = {

    'title':'this is DonateMoney'

        }
    return render(request,'donatemoney.html',contaxt)

def donateclothes(request):
    contaxt = {

    'title':'this is DonateClothes'

        }
    return render(request,'donateclothes.html',contaxt)

def take (request):
    contaxt = {

    'title':'this is Take'

        }
    return render(request,'take.html',contaxt)