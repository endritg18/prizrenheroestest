from django.views.generic import ListView, DetailView,CreateView,UpdateView,DeleteView
from django.shortcuts import render, get_object_or_404

from django.contrib.auth.models import User
from .models import Products,Post
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

class ProductListView(ListView):
    queryset = Products.objects.all()
    template_name = "list.html"


def product_list_view(request):
    queryset = Products.objects.all()
    context = {
        'object_list': queryset
    }
    return render(request, "list.html", context)

class ProductDetailView(DetailView):
    queryset = Products.objects.all()
    template_name = "detail.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProductDetailView, self).get_context_data(*args, **kwargs)
        print(context)

        return context


def product_detail_view(request, pk=None, *args, **kwargs):
    #instance = Product.objects.get(pk=pk) #id
    instance = get_object_or_404(Products, pk=pk)
    context = {
        'object': instance
    }
    return render(request, "detail.html", context)

class PostListView(ListView):
    model = Post
    template_name = 'home_page.html'  # <app>/<model>_<viewtype>.html
    context_object_name = 'posts'
    ordering = ['-date_posted']
    paginate_by = 5


class UserPostListView(ListView):
    model = Post
    template_name = 'user_posts.html'  # <app>/<model>_<viewtype>.html
    context_object_name = 'posts'
    paginate_by = 5

    def get_queryset(self):
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        return Post.objects.filter(author=user).order_by('-date_posted')


class PostDetailView(DetailView):
    model = Post
    template_name ='post_detail.html'



class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    template_name ='post_form.html'
    fields = ['title', 'content','image']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['title', 'content']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = '/'

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False
