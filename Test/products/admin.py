from django.contrib import admin

# Register your models here.

from .models import Products,Post

admin.site.register(Products)

admin.site.register(Post)